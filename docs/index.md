# Dominoes

## Introduction

The [Dominoes](https://gitlab.com/hqv1/dominoes) project is a pet project of mine. It serves the following purposes.

* Allows me to design and develop for fun
* Uses the latest .NET Core technologies
* Keeps me updated in interesting software methodology
* Keeps me updated in interesting technology
* Focused on cloud based solutions
* Keep the cost low
* Production grade code
* Allows me to keep a handy toolkit in one place.

Dominoes have many variations and the one I play the most is called [All Fives](https://www.pagat.com/tile/wdom/all_fives.html). A variation to it is we don't play teams.

## Objectives

The objective is to build a production-grade dominoes game; though I don't expect any volume whats so ever.

The service does the following.

* Add players
* Start scoring
* Start a game
* Play the game
* Complete a game
* Complete scoring

The dominoes game will consist of various micro-services and APIs connected by queues. The details are still ongoing.

## Milestones

| Name                                | Description                                                                                                                                                                                                     | Status    |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Create POC Command Line App         | Create a simple command line app to insert a player. Gets us familiar with .NET Core and some patterns. Pick a data store.                                                                                      | Completed |
| Create API to create/get player     | Create a barebone API to create/get player. Gets us familiar with WEB API and allows us to create players.                                                                                                      | Completed |
| Host API                            | Understand how to host an API. Learn about cloud solutions.                                                                                                                                                     | To Do     |
| Split API into API and microservice | The API receives an Add Player request and drops it into a queue. A service picks it up and inserts the player into and generates another event. Understand the different queues and micro-service technology.  | To Do     |
| Host the services                   | Understand how to host a micro-service                                                                                                                                                                          | To Do     |