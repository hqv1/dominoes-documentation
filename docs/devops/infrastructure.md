# Infrastructure

## Introduction

This document describes the various infrastructure that makes up Dominoes and why they were chosen.

## Key Vault

I'm using [Azure Key Vault](https://azure.microsoft.com/en-us/services/key-vault/) to store my secrets. It supports [loading app secrets on startup](https://docs.microsoft.com/en-us/aspnet/core/security/key-vault-configuration). AWS System Manager also has a [.NET Core configuration provider](https://aws.amazon.com/blogs/developer/net-core-configuration-provider-for-aws-systems-manager/);

## Database

Our database is on [AWS RDS for PostgreSQL](https://aws.amazon.com/rds/postgresql/). I use PostgreSQL in my day job and its a great open-source database. Why AWS when everything else is on Azure? I was no longer eligible for the free tier on Azure and the cheapest database would cost me about 22 dollars a month. AWS free tier lasts for a year.  Afterward AWS allows you to [pause the database](https://aws.amazon.com/about-aws/whats-new/2017/06/amazon-rds-supports-stopping-and-starting-of-database-instances/) when not in use, saving you additional money. Azure doesn't allow [pausing the database](https://stackoverflow.com/questions/26986213/stopping-sql-azure-db-when-not-in-use).

This is a hobby project and I don't expect any volume so I'm fine having my resources spread across different cloud provider.

## Kubernetes

I wanted all of my applications to be running on [Azure Kubernetes Service](https://azure.microsoft.com/en-us/services/kubernetes-service/). But I wanted to take baby steps. So first I had the Web App running on Azure App Service but I couldn't get GitLab CI to work with Azure App Service (see below). This forced me to jump ahead.

To create an AKS cluster run `az aks create -g dominoes -n dominoes-cluster --node-count 1 --node-vm-size Standard_B2s`. This will create 1 node with the cheapest VM.

## GitLab CI to Kubernetes

There's two steps to integrate GitLab CI to Kubernetes; creating the integration and deployment. I found this [stack overflow answer](https://stackoverflow.com/questions/50749095/how-to-integrate-gitlab-ci-w-azure-kubernetes-kubectl-acr-for-deployments) extremely helpful.

To get my Kubernetes cluster login credentials `az aks get-credentials --resource-group Dominoes --name dominoes-cluster`

### Create the integration - Cluster Admin Service Account

The big piece that the stack overflow skipped on was creating a service account with `cluster-admin` role and having GitLab use it. Here's the steps

#### Create a Service Account and bind a cluster role to it

The following command will create a new service account and bind the `cluster-admin` role to it. `kubectl apply -f .\src\kubernetes\serviceaccount_gitlab.yaml`.

#### Get the CA and token of the service account

Get the secret location
`kubectl.exe get sa gitlab-sa -o yaml`

Get the binary64 strings of the CA and token
`kubectl get secret gitlab-sa-token-xxxxx -o yaml`

Copy the binary64 strings to a file and decode them (on Windows machines)
`certutil -decode data.b64 data.txt`

#### Use the information to populate the GitLab Kubernetes form

You should be able to install Helm now.

## Kubernetes and Key Vault

Azure AKS does not have native support for Key Vault. We'll be using [AAD Pod Identity](https://github.com/Azure/aad-pod-identity) to give our pods an identity that has authorization to read from our Key Vault. There's an [example](https://pumpingco.de/blog/use-azure-keyvault-with-asp-net-core-running-in-an-aks-cluster-using-aad-pod-identity/) of it working. See also option 2 in this [document](https://github.com/Azure/kubernetes-keyvault-flexvol). But remember that we don't need a Flex Volume as we'll be interacting with the Key Vault directly.

## Old - Web App as an Azure App Service

### Web App as an App Service

The Dominoes Web App is hosted on [Azure App Service](https://azure.microsoft.com/en-us/services/app-service/). I'm using their free tier. The integration with Visual Studio is top-notch. It's very easy to publish to the App Service. A bit too easy. I haven't had to touch and CI/CD yet.

### Azure App Service to Key Vault Integration

The following steps are required to allow an Azure App Service to access an Azure Key Vault.

* The Azure App Service is using Managed Identity to access the Azure Key Vault. We'll need to give the App Service a system assigned identity. Go into `App Serivce -> Settings -> Identity -> System assigned`. Set the Status to `On`.
* Next we need to assign a policy in Key Vault. Go into `Key Vault -> Settings -> Access policies`. Click on `Add Access Policy`. Use the `Key, Secret, & Certificate Management` template and and find the App Service in `Authorized application`. Save.

### Web App to Database Integration

The following steps are required to get the Azure App Service to access the AWS RDS database.

* Use [this](https://docs.microsoft.com/en-us/azure/app-service/overview-inbound-outbound-ips) to get the list of the App Service outbound IPs.
* Go into the RDS Security Group. In the `Inbound` tab, Edit and Add the outbound IPs.

### GitLab to App Service CI

And this is where the App Service adventure ends. I couldn't get GitLab CI to work with App Service. There's examples online, but it doesn't seem to work for me. That's okay, because I wanted the Web App to run on Kubernetes. 