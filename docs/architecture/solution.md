# Solution Architecture

## Introduction

These are a few solution level architecture and designs I decided on.

## Single Solution

For now, all projects are in a single solution. It's easier to view and update all code if it sits in a single solution. Because this is a one man operation, I may keep it as is. Though I was expecting to split the projects up, hence the solution name and location.

## Projects

We have the following projects.

* SeedWork : contains a small subset of common and reusable code. Also contains the business models.
* Data : contains my data access code. It's Entity Framework core.
* Core : contains business logic
* Hosts : Abstraction of common Host (e.g. Console, Web API) functionality.
* ConsoleHost: Dominoes Console application
* WebApi: Dominoes Web API
