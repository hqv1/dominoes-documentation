# .Net Core

## Introduction

I'm using the latest .Net Core (3.x) and .Net Standard (2.1). The following are some interesting points.

## Nullable reference types

[Nullable references](https://docs.microsoft.com/en-us/dotnet/csharp/nullable-references) is one of the big changes that came with .Net Core 3.x. To add it into your project use this [tutorial](https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/nullable-reference-types).

Entity Framework has some [recommendations](https://docs.microsoft.com/en-us/ef/core/miscellaneous/nullable-reference-types) when being used with Nullable reference types.

I'm enjoying having Nullable reference types. It's annoying in existing projects because there's a lot of cleanup involved. You'll be surprise by the amount of possible null exceptions in your code. But once you clean them up, your code will be safer and cleaner.

## Entity Framework Core

I'm using [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) for data access. I'm mostly used micro-ORMs in production environment and I wanted to see what EF Core had to offer.

I chose to do [database-first design](https://entityframework.net/code-first-vs-database-first). I'm comfortable working in SQL and [migrations in EF Core code first is wonky](https://www.thereformedprogrammer.net/handling-entity-framework-core-database-migrations-in-production-part-1/). I expect to have a lot of churn in the beginning and it's easier to update the database and then have the code match the changes rather than the other way around.

## Microsoft Libraries for Command Line applications

I followed the way .NET Core Web APIs does it's configuration in my command line applications.

This means using the following libraries

* Microsoft.Extensions.Configuration
* Microsoft.Extensions.Configuration.Json
* Microsoft.Extensions.DependencyInjection
* Microsoft.Extensions.Logging

With these libraries, I get Microsoft's configuration builder, dependency injection, and logging frameworks. I can then extract common functionality between the command line application and Web Api into a shared project.

## Logging

I use [Serilog](https://serilog.net/) to write logs. I enjoy writing and viewing structured loggers and enrichment is a boon. There are a tons of sinks to write your outputs to.

## MediatR

I find [MediatR](https://github.com/jbogard/MediatR) and the [mediator pattern](https://en.wikipedia.org/wiki/Mediator_pattern) to be extremely useful for the following reasons.

* Decoupling the interactions between objects, especially when that object owns a lot of unrelated responsibilities (e.g. Command Line parsers).
* Helps promote the [Open-closed principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle).

I use the mediator pattern in three places.

* Handling a business event
* Command line parsing
* In the Web API Controller. This one may be a bit too much and I may remove it in the future.

## AutoMapper

[Automapper](https://automapper.org/) is a convention base tool to map one object to another. I do a lot of mapping (maybe too much mapping) and its an invaluable tool.