# Docker

## Knowledge Base

* [Dockerize an existing ASP.NET Core application](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/docker/building-net-docker-images?view=aspnetcore-3.1)
* [Microsoft Dotnet Docker tutorial repository](https://github.com/dotnet/dotnet-docker)

## Gotchas

### Azure Managed Identity doesn't work inside a local Docker container

This means we can't authenticate to Azure Key Vault using Azure Managed Identity. [Here's](https://stackoverflow.com/questions/54880080/azure-managed-identity-from-within-a-docker-container-running-locally) more discussion and workarounds.

What I did was run the command `az account get-access-token --resource=https://vault.azure.net | jq -r .accessToken` to get the access token. Then added it into `appsettings.Development.json` so that it could be picked up by the application. I also removed the file from being tracked in git.
